import React, { Component } from "react";
import PropTypes from "prop-types";

export class Search extends Component {
  state = {
    text: "",
  };

  static propTypes = {
    searchUsers: PropTypes.func.isRequired,
    clearUsers: PropTypes.func.isRequired,
    showClear: PropTypes.bool.isRequired,
    setAlert: PropTypes.func.isRequired,
  };

  onChange = (e) => {
    this.setState({ text: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    if (this.state.text === "") {
      this.props.setAlert("Please Enter Something", "light");
    } else {
      this.props.searchUsers(this.state.text);
      this.setState({ text: "" });
      // console.log(this.state.text);
    }
  };
  render() {
    const { showClear, clearUsers } = this.props;
    return (
      <div>
        <form className="form">
          <input
            type="text"
            name="text"
            placeholder="Search Users.."
            value={this.state.text}
            onChange={this.onChange}
          />
          <button
            onClick={this.onSubmit}
            type="submit"
            className="btn btn-dark btn-block"
          >
            Search
          </button>
          {showClear && (
            <button
              className="btn btn-light btn-block my-1"
              onClick={clearUsers}
            >
              Reset
            </button>
          )}
        </form>
      </div>
    );
  }
}

export default Search;
