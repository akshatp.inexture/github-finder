import React from "react";
import PropTypes from "prop-types";

const Navbar = (props) => {
  return <nav className="navbar bg-primary">{props.title}</nav>;
};

Navbar.defaultProps = {
  title: "Github Finder",
};
Navbar.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Navbar;
